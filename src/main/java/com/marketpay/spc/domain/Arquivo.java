package com.marketpay.spc.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "arquivo")
@Data
public class Arquivo {

    @Id
    @SequenceGenerator(name="arquivo_id_seq",
            sequenceName="arquivo_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="arquivo_id_seq")
    private Long id;

    @NotNull
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "origin_filename_regex")
    private String originFilenameRegex;

    @NotNull
    @Column(name = "dest_filename")
    private String destFilename;

    @NotNull
    @Column(name = "origin_dir")
    private String originDir;

    @NotNull
    @Column(name = "dest_dir")
    private String destDir;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_origin_sftp")
    private Sftp originSftp;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_dest_sftp")
    private Sftp destSftp;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_layout")
    private Layout layout;


}
