package com.marketpay.spc.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "layout")
@Data
public class Layout {

    @Id
    @SequenceGenerator(name="layout_id_seq",
            sequenceName="layout_id_seq",
            allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator="layout_id_seq")
    private Long id;

    @NotNull
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "lineLength")
    private Integer lineLength;

}
