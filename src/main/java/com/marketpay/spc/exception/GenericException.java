package com.marketpay.spc.exception;

import com.marketpay.spc.dto.response.GenericResponse;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class GenericException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private HttpStatus httpStatus;

    private GenericResponse response;

    public GenericException(HttpStatus httpStatus, String descricao) {
        super(descricao);
        this.httpStatus = httpStatus;
        response = new GenericResponse(descricao);
    }
    public GenericException(HttpStatus httpStatus, String descricao, List<String> erros) {
        super(descricao);
        this.httpStatus = httpStatus;
        response = new GenericResponse(descricao, erros);
    }
    public ResponseEntity<?> build() {
        return ResponseEntity.status(httpStatus).body(response);
    }
}
