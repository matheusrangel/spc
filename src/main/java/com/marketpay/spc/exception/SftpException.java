package com.marketpay.spc.exception;


public class SftpException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public SftpException(String mensagem) {
        super(mensagem);
    }

}
