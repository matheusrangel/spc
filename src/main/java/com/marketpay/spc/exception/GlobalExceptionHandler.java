package com.marketpay.spc.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.marketpay.spc.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author CONDUCTOR\matheus.rangel
 * Essa classe captura as exceções lançadas na aplicação que são chamadas 
 * a partir de um Controller Spring.
 * Ela elimina a necessidade de tratar as exceções individualmente nos Controllers.
 * Para adicionar um novo tratamento de exceção, basta adicionar um método anotado
 * por {@link ExceptionHandler} que recebe como argumento a exceção a ser tratada.
 */
@ControllerAdvice
@Component
public class GlobalExceptionHandler {

    @Autowired
    private MessageService messageService;

    /**
     * Trata as exceções lançadas pela base de violação de Constraints.
     * Retorna Bad Request por assumir que o cliente enviou dados inválidos que causaram o erro.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(ConstraintViolationException ex) {
        List<String> msgs = ex.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        return new GenericException(
                HttpStatus.BAD_REQUEST, msgs.toString())
                .build();
    }

    /**
     * Trata os erros lançadas quando as anotações de validação (ex: @NotNull, @Size) são violadas.
     * Criada essencialmente para tratar os erros de validação das requests.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(MethodArgumentNotValidException ex) {
        List<String> erros = new ArrayList<>();
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        fieldErrors.forEach(e ->
                erros.add(e.getField() + ": " + e.getDefaultMessage()));

        return new GenericException(
                HttpStatus.BAD_REQUEST,
                messageService.getMessage("exception.validation-erros"),
                erros)
                .build();
    }

    /**
     * Trata a exceção customizada {@link RegraDeNegocioException}. 
     * Essa exceção deve ser lançada quando uma quebra de regra de negócio acontece.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(RegraDeNegocioException ex) {
        return new GenericException(
                HttpStatus.BAD_REQUEST, ex.getMessage())
                .build();
    }

    /**
     * Trata a exceção {@link ServletRequestBindingException}.
     * Essa exceção é lançada pelo Spring nas primeiras fases de validação do request.
     * Geralmente é lançada quando um header obrigatório não foi enviado pelo cliente.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(ServletRequestBindingException ex) {
        return new GenericException(
                HttpStatus.BAD_REQUEST, ex.getMessage())
                .build();
    }

    /**
     * Trata a exceção {@link HttpMessageNotReadableException}.
     * Essa exceção é lançada pelo sistema quando não foi possível converter o request Http.
     * Essa exceção também é wrapper da exceção {@link JsonMappingException}. Exceção muito comum, 
     * que é lançada quando existe erro de conversão no Json da Request.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(HttpMessageNotReadableException ex) {
        if (ex.getCause() instanceof JsonMappingException) {
            JsonMappingException jsonMapEx = (JsonMappingException) ex.getCause();
            List<String> erros = new ArrayList<String>();
            jsonMapEx.getPath().forEach(r ->
                    erros.add(r.getFieldName() + ": " + messageService.getMessage("exception.json-mapping")));

            return new GenericException(
                    HttpStatus.BAD_REQUEST,
                    messageService.getMessage("exception.validation-erros"),
                    erros)
                    .build();
        }

        return new GenericException(
                HttpStatus.BAD_REQUEST, messageService.getMessage("exception.http-message-convertion"))
                .build();
    }

    /**
     * Trata a exceção customizada {@link GenericException}.
     * Essa exceção é basicamente um Dto para servir como response
     * no caso de erros serem retornados ao cliente.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(GenericException ex) {
        return ex.build();
    }

    /**
     * Handler genérico que captura qualquer exceção lançada e que não foi tratada individualmente.
     * Por essencialmente capturar erros não previstos, foi assumido que ocorreu um erro interno,
     * por isso a resposta Http Internal Server Error.
     * Porém, existe possibilidade de outros tipos de exceção que não foram tratados caiam nesse handler.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(Exception ex) {
        return new GenericException(
                HttpStatus.INTERNAL_SERVER_ERROR,
                messageService.getMessage("exception.unknown"))
                .build();
    }


    /**
     * Trata a exceção customizada {@link RecursoNaoEncontradoException}.
     * Essa exceção deve ser lançada quando é feita uma busca por um recurso
     * e o mesmo não existe. (ex: Busca por um Id inexistente.)
     * Por essa razão, a resposta Http é Not Found.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(RecursoNaoEncontradoException ex) {
        return new GenericException(
                HttpStatus.NOT_FOUND, ex.getMessage())
                .build();
    }

    /**
     * Trata a exceção {@link HttpRequestMethodNotSupportedException}.
     * Essa exceção deve ser lançada quando é feita uma requisição para um serviço existente,
     * mas utilizando o método Http incorreto. (ex: Enviando um PATCH para um endpoint que 
     * aceita apenas PUT.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(HttpRequestMethodNotSupportedException ex) {
        return new GenericException(
                HttpStatus.METHOD_NOT_ALLOWED,
                messageService.getMessage("exception.http-method-not-allowed"))
                .build();
    }

    /**
     * Trata a exceção {@link HttpMediaTypeNotSupportedException}.
     * Essa exceção é lançada quando é feita uma requisição para um serviço existente,
     * mas enviando um Content-Type não suportado.
     */
    @ExceptionHandler
    public ResponseEntity<?> handle(HttpMediaTypeNotSupportedException ex) {
        return new GenericException(
                HttpStatus.UNSUPPORTED_MEDIA_TYPE,
                messageService.getMessage("exception.http-media-type-not-allowed"))
                .build();
    }


}