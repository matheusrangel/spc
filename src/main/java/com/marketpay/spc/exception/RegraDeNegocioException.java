package com.marketpay.spc.exception;

public class RegraDeNegocioException extends RuntimeException {
    private static final long serialVersionUID = 7248947240818221194L;
    public RegraDeNegocioException(String mensagem) {
        super(mensagem);
    }

}