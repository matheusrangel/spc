package com.marketpay.spc.infra.persistence.repository;

import com.marketpay.spc.domain.Arquivo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArquivoRepository extends JpaRepository<Arquivo, Long> {

}
