package com.marketpay.spc.infra.persistence.repository;

import com.marketpay.spc.domain.Arquivo;
import com.marketpay.spc.domain.Layout;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LayoutRepository extends JpaRepository<Layout, Long> {

}
