package com.marketpay.spc.infra.persistence.repository;


import com.marketpay.spc.domain.Sftp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SftpRepository extends JpaRepository<Sftp, Long> {

}
