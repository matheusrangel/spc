package com.marketpay.spc.service;

import com.marketpay.spc.domain.Arquivo;
import com.marketpay.spc.dto.request.ArquivoCadastroRequest;
import com.marketpay.spc.dto.response.ArquivoCadastroResponse;
import com.marketpay.spc.infra.persistence.repository.ArquivoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArquivoService {

    @Autowired
    private ArquivoRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    public ArquivoCadastroResponse cadastrar(ArquivoCadastroRequest request) {
        Arquivo arquivo = modelMapper.map(request, Arquivo.class);
        arquivo = repository.save(arquivo);
        return modelMapper.map(arquivo, ArquivoCadastroResponse.class);
    }

    public Optional<Arquivo> findById(Long id) {
        return repository.findById(id);
    }

}
