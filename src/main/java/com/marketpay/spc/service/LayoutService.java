package com.marketpay.spc.service;

import com.marketpay.spc.domain.Layout;
import com.marketpay.spc.dto.request.LayoutCadastroRequest;
import com.marketpay.spc.infra.persistence.repository.LayoutRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LayoutService {

    @Autowired
    private LayoutRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    public Layout cadastrar(LayoutCadastroRequest layoutCadastroRequest) {
        Layout layout = modelMapper.map(layoutCadastroRequest, Layout.class);

        return repository.save(layout);
    }

}
