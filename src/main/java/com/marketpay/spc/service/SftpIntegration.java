package com.marketpay.spc.service;

import com.marketpay.spc.domain.Sftp;
import com.sshtools.net.SocketTransport;
import com.sshtools.sftp.*;
import com.sshtools.ssh.*;
import com.sshtools.ssh2.Ssh2Client;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SftpIntegration {

    private final static String PARENT_DIRECTORY_COMMAND = "..";
    private final static String CURRENT_DIRECTORY_COMMAND = ".";

    private Sftp ftp;

    private SshClient ssh = null;
    private SftpClient sftpClient;

    private int status;

    public SftpIntegration(Sftp dados) {
        this.ftp = dados;
    }

    public Sftp getFtp() {
        return ftp;
    }

    public void setFtp(Sftp ftp) {
        this.ftp = ftp;
    }

    public boolean conectar() throws IOException, SftpStatusException, SshException, ChannelOpenException {
        boolean isConnected = ssh != null && ssh.isConnected();
        if (!isConnected) {
            SshConnector con = SshConnector.createInstance();

            SocketTransport t = new SocketTransport(ftp.getHost(), Integer.valueOf(ftp.getPort()));
            t.setTcpNoDelay(true);

            ssh = con.connect(t, ftp.getUsername(), true);

            Ssh2Client ssh2 = (Ssh2Client) ssh;

//            ssh = new SshClient();
//            if (ftp.getPorta() != null) {
//                ssh.connect(ftp.getHost(), ftp.getPorta(), new IgnoreHostKeyVerification());
//            } else {
//                ssh.connect(ftp.getHost(), new IgnoreHostKeyVerification());
//            }

            PasswordAuthentication pwd = new PasswordAuthentication();
            pwd.setUsername(ftp.getUsername());
            pwd.setPassword(ftp.getPassword());
            status = ssh.authenticate(pwd);
            isConnected = status == SshAuthentication.COMPLETE;
            sftpClient = new SftpClient(ssh2);
        }
        return isConnected;
    }

    public boolean desconectar() {
        boolean wasLogout = false;
        if (ssh != null && ssh.isConnected()) {
            ssh.disconnect();
            // sftpClient.quit();
            wasLogout = true;

        }
        return wasLogout;
    }

    public boolean enviarArquivo(String caminhoArquivoLocal, String caminhoArquivoRemoto)
            throws IOException, SftpStatusException, SshException, ChannelOpenException, TransferCancelledException {
        boolean done = false;
        InputStream inputStream = null;
        try {
            if (conectar()) {
                File arquivoLocal = new File(caminhoArquivoLocal);

                inputStream = new FileInputStream(arquivoLocal);
                sftpClient.cd(caminhoArquivoRemoto);
                sftpClient.put(inputStream, arquivoLocal.getName());
                done = true;

                if (done) {
                    System.out.println("The first file is uploaded successfully.");
                }
            }

        } catch (IOException ex) {
            done = false;
            desconectar();
            throw ex;
        } finally {
            if (inputStream != null) inputStream.close();
        }
        return done;
    }

    public boolean receberArquivo(String caminhoArquivoRemoto, String caminhoArquivoLocal) throws Exception {
        boolean done = false;
        try {
            conectar();
            SftpFileAttributes atributosRemoto = listarArquivoRemoto(caminhoArquivoRemoto);
            if (atributosRemoto == null) {
                throw new FileNotFoundException(caminhoArquivoRemoto);
            }
            File arquivoLocal = new File(caminhoArquivoLocal);
            OutputStream outputStream = new FileOutputStream(arquivoLocal);

            System.out.println("Start download first file");
            SftpFileAttributes fileAttributes = sftpClient.get(caminhoArquivoRemoto, outputStream);

            done = fileAttributes != null;
            outputStream.close();

        } catch (IOException | SftpStatusException | SshException | ChannelOpenException ex) {
            desconectar();
            throw ex;
        }
        return done;
    }

    public List<SftpFile> listarArquivosRemoto(String caminhoDiretorioRemoto) throws Exception {
        List<SftpFile> files = new ArrayList<>();

        if (conectar()) {
            criarPasta(caminhoDiretorioRemoto);
            List<SftpFile> conteudosRemoto = Arrays.asList(sftpClient.ls(caminhoDiretorioRemoto));
            for (SftpFile conteudoRemoto : conteudosRemoto) {
                if (conteudoRemoto.getFilename().equals(CURRENT_DIRECTORY_COMMAND)
                        || conteudoRemoto.getFilename().equals(PARENT_DIRECTORY_COMMAND)) {
                    // skip parent directory and the directory itself
                    continue;
                }
                files.add(conteudoRemoto);
            }
        }


        return files;
    }

    public ArrayList<String> listarNomeArquivosRemoto(String caminhoDiretorioRemoto) throws Exception {
        ArrayList<String> files = new ArrayList<>();
        try {
            conectar();
            List<SftpFile> listarArquivosRemoto = listarArquivosRemoto(caminhoDiretorioRemoto);
            for (SftpFile sftpFile : listarArquivosRemoto) {
                files.add(sftpFile.getFilename());
            }

        } catch (IOException ex) {
            desconectar();
            throw ex;
        }
        return files;
    }

    public boolean moverArquivoRemoto(String arquivoOrigem, String arquivoDestino) throws Exception {
        boolean done = false;
        try {
            conectar();
            criarPastasRemota(arquivoDestino);
            sftpClient.rename(arquivoOrigem, arquivoDestino);
            done = true;
        } catch (IOException | SftpStatusException | SshException | ChannelOpenException ex) {
            done = false;
            desconectar();
            throw ex;
        }
        return done;
    }

    public boolean renomearArquivoRemoto(String localArquivoRemoto, String nomeAntigo, String nomeNovo) throws Exception {
        boolean done = false;
        try {
            conectar();
            if(hasRemoteFile(localArquivoRemoto+nomeNovo)) {
                sftpClient.rm(localArquivoRemoto+nomeNovo);
            }
            sftpClient.rename(localArquivoRemoto+nomeAntigo, localArquivoRemoto+nomeNovo);
            done = true;
        } catch (IOException | SftpStatusException | SshException | ChannelOpenException ex) {
            done = false;
            desconectar();
            throw ex;
        }
        return done;
    }

    public boolean excluirArquivoRemoto(String arquivoRemoto) throws Exception {
        boolean done = false;
        try {
            conectar();
            sftpClient.rm(arquivoRemoto);
            done = true;
        } catch (IOException | SftpStatusException | SshException | ChannelOpenException ex) {
            done = false;
            desconectar();
            throw ex;
        }
        return done;
    }

    public boolean excluirPastaRemota(String pastaRemota) throws Exception {
        boolean done = false;
        try {
            conectar();
            done = excluirArquivoRemoto(pastaRemota);
        } catch (IOException | SftpStatusException | SshException | ChannelOpenException ex) {
            desconectar();
            throw ex;
        }
        return done;
    }

    public void criarPastasRemota(String caminho) throws Exception {
        try {
            if (conectar()) {
                StringBuffer caminhoParcial = new StringBuffer();
                String[] subCaminhos = caminho.split("/");

                for (String subCaminho : subCaminhos) {
                    if (caminhoParcial.length() > 0) {
                        caminhoParcial.append("/");
                    }
                    caminhoParcial.append(subCaminho);
                    criarPasta(caminhoParcial.toString());
                }
            }

        } catch (IOException | SftpStatusException | SshException | ChannelOpenException ex) {
            desconectar();
            throw ex;
        }
    }

    public SftpFileAttributes listarArquivoRemoto(String caminho) {
        SftpFileAttributes fileAttributes;
        try {
            fileAttributes = sftpClient.stat(caminho);
        } catch (SftpStatusException | SshException e) {
            return null;
        }
        return fileAttributes;
    }

    public Optional<SftpFile> listarArquivoRemoto(String caminho, String regex) throws Exception {
        List<SftpFile> files;
        files = listarArquivosRemoto(caminho);
        return files.stream()
                .filter(f -> f.getFilename().matches(regex))
                .findFirst();
    }



    public boolean hasRemoteFile(String path) {
        return listarArquivoRemoto(path) != null;
    }

    private boolean criarPasta(String caminho) throws Exception {
        boolean done = false;
        try {
            SftpFileAttributes arquivoRemoto = listarArquivoRemoto(caminho);

            boolean isPastaExistente = false;
            if (arquivoRemoto != null || caminho.contains(".")) {
                isPastaExistente = true;
            }
            if (!isPastaExistente) {
                sftpClient.mkdir(caminho);
                done = true;
            }
        } catch (SftpStatusException | SshException ex) {
            desconectar();
            throw ex;
        }
        return done;
    }

    /**
     * Removes a non-empty directory by delete all its sub files and sub directories
     * recursively. And finally remove the directory.
     * @throws Exception
     */
    public boolean excluirPastaRecursivamente(String pastaPai, String pastaAtual) throws Exception {
        String dirToList = pastaPai;
        boolean done = false;
        if (!pastaAtual.equals("")) {
            dirToList += "/" + pastaAtual;
        }

        List<SftpFile> subFiles = listarArquivosRemoto(dirToList);

        if (subFiles != null && subFiles.size() > 0) {
            for (SftpFile aFile : subFiles) {
                String currentFileName = aFile.getFilename();
                if (currentFileName.equals(CURRENT_DIRECTORY_COMMAND)
                        || currentFileName.equals(PARENT_DIRECTORY_COMMAND)) {
                    // skip parent directory and the directory itself
                    continue;
                }
                String filePath = pastaPai + "/" + pastaAtual + "/" + currentFileName;
                if (pastaAtual.equals("")) {
                    filePath = pastaPai + "/" + currentFileName;
                }

                if (aFile.isDirectory()) {
                    // remove the sub directory
                    excluirPastaRecursivamente(dirToList, currentFileName);
                } else {
                    // delete the file
                    boolean deleted = excluirArquivoRemoto(filePath);
                    if (deleted) {
                        System.out.println("DELETED the file: " + filePath);
                    } else {
                        System.out.println("CANNOT delete the file: " + filePath);
                    }
                }
            }

            // finally, remove the directory itself
        }
        try {
            sftpClient.rm(dirToList);
            done = true;
        } catch (Exception e) {
            done = false;
        }

        return done;
    }
}
