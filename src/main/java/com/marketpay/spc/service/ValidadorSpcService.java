package com.marketpay.spc.service;

import com.marketpay.spc.domain.Arquivo;
import com.marketpay.spc.domain.Layout;
import com.marketpay.spc.exception.RecursoNaoEncontradoException;
import com.marketpay.spc.exception.RegraDeNegocioException;
import com.marketpay.spc.exception.SftpException;
import com.sshtools.sftp.SftpFile;
import com.sshtools.sftp.SftpStatusException;
import com.sshtools.sftp.TransferCancelledException;
import com.sshtools.ssh.ChannelOpenException;
import com.sshtools.ssh.SshException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class ValidadorSpcService {

    @Autowired
    private EmailService emailService;

    public void executarRotina(Arquivo arquivo) throws Exception {
        SftpIntegration sftpOrigin = new SftpIntegration(arquivo.getOriginSftp());
        SftpFile sftpFile = buscarArquivo(arquivo, sftpOrigin);
        copiarArquivoParaServidor(sftpFile, arquivo, sftpOrigin);
        validarLayout(arquivo);
        enviarParaSftpDestino(arquivo);
    }

    private void copiarArquivoParaServidor(SftpFile sftpFile, Arquivo arquivo, SftpIntegration sftpOrigin) throws Exception {
        sftpOrigin.receberArquivo(
                sftpFile.getAbsolutePath(),
                System.getProperty("user.dir")+"/" + arquivo.getDestFilename());
    }

    private void enviarParaSftpDestino(Arquivo arquivo) {
        SftpIntegration sftpDest = new SftpIntegration(arquivo.getDestSftp());

        try {
            sftpDest.enviarArquivo(
                    System.getProperty("user.dir")+"/"+arquivo.getDestFilename(),
                    arquivo.getDestSftp().getDirectory() + arquivo.getDestDir() );
        } catch (IOException | SftpStatusException | SshException | ChannelOpenException | TransferCancelledException e) {
            throw new SftpException(e.getMessage());
        }
    }

    private void validarLayout(Arquivo arquivo) {
        Layout layout = arquivo.getLayout();

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    System.getProperty("user.dir")+"/"+arquivo.getDestFilename()));
            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                if (line.length() != layout.getLineLength()) {
                    emailService.sendSimpleMessage(
                            "matheus.rangel@conductor.com.br",
                            "[ARQUIVO " + arquivo.getDescription() + " INVALIDO]",
                            "[ARQUIVO " + arquivo.getDescription() + " INVALIDO]");
                    throw new RegraDeNegocioException("ARQUIVO: " + arquivo.getDescription() + " INVALIDO!");
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private SftpFile buscarArquivo(Arquivo arquivo, SftpIntegration sftpOrigin) throws Exception {
        List<SftpFile> filenames = sftpOrigin.listarArquivosRemoto(
                arquivo.getOriginSftp().getDirectory() +"/"+ arquivo.getOriginDir());

        Pattern pattern =   Pattern.compile(arquivo.getOriginFilenameRegex(), Pattern.CASE_INSENSITIVE);
        Optional<SftpFile> fileFound = filenames.stream().findFirst().filter(f -> pattern.matcher(f.getFilename()).find());

        if (!fileFound.isPresent()) {
            emailService.sendSimpleMessage(
                    "matheus.rangel@conductor.com.br",
                    "[ARQUIVO " + arquivo.getDescription() + " NAO ENCONTRADO]",
                    "[ARQUIVO " + arquivo.getDescription() + " NAO ENCONTRADO]");

            throw new RecursoNaoEncontradoException("Arquivo nao localizado");
        }

        return fileFound.get();
    }


}
