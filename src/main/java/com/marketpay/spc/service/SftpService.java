package com.marketpay.spc.service;

import com.marketpay.spc.domain.Sftp;
import com.marketpay.spc.dto.request.SftpCadastroRequest;
import com.marketpay.spc.infra.persistence.repository.SftpRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SftpService {

    @Autowired
    private SftpRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    public Sftp cadastrar(SftpCadastroRequest sftpRequest) {
        Sftp sftp = modelMapper.map(sftpRequest, Sftp.class);

        return repository.save(sftp);
    }

}
