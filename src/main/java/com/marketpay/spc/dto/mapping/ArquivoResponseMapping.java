package com.marketpay.spc.dto.mapping;


import com.marketpay.spc.domain.Arquivo;
import com.marketpay.spc.dto.response.ArquivoCadastroResponse;
import org.modelmapper.PropertyMap;

public class ArquivoResponseMapping extends PropertyMap<Arquivo, ArquivoCadastroResponse> {

    @Override
    protected void configure() {
        map().setIdOriginSftp(source.getOriginSftp().getId());
        map().setIdDestSftp(source.getDestSftp().getId());
        map().setIdLayout(source.getLayout().getId());
    }
}
