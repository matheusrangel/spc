package com.marketpay.spc.dto.mapping;

import com.marketpay.spc.domain.Arquivo;
import com.marketpay.spc.domain.Layout;
import com.marketpay.spc.domain.Sftp;
import com.marketpay.spc.dto.request.ArquivoCadastroRequest;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

public class ArquivoRequestMapping extends PropertyMap<ArquivoCadastroRequest, Arquivo> {

    @Override
    protected void configure() {
        skip(destination.getId());
        using(sftpConverter).map(source.getIdOriginSftp(), destination.getOriginSftp());
        using(sftpConverter).map(source.getIdDestSftp(), destination.getDestSftp());
        using(layoutConverter).map(source.getIdLayout(), destination.getLayout());
    }

    private Converter<Long, Sftp> sftpConverter = new AbstractConverter<Long, Sftp>() {
        @Override
        protected Sftp convert(Long idSource) {
            Sftp sftp = new Sftp();
            sftp.setId(idSource);

            return sftp;
        }
    };

    private Converter<Long, Layout> layoutConverter = new AbstractConverter<Long, Layout>() {
        @Override
        protected Layout convert(Long idSource) {
            Layout layout = new Layout();
            layout.setId(idSource);

            return layout;
        }
    };

}
