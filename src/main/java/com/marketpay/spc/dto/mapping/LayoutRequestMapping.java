package com.marketpay.spc.dto.mapping;

import com.marketpay.spc.domain.Layout;
import com.marketpay.spc.dto.request.LayoutCadastroRequest;
import org.modelmapper.PropertyMap;

public class LayoutRequestMapping extends PropertyMap<LayoutCadastroRequest, Layout> {
    @Override
    protected void configure() {

    }
}
