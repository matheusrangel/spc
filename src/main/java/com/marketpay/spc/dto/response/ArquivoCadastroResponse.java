package com.marketpay.spc.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArquivoCadastroResponse {

    private Long id;
    private String description;
    private String originFilenameRegex;
    private String destFilename;
    private String originDir;
    private String destDir;
    private Long idOriginSftp;
    private Long idDestSftp;
    private Long idLayout;

}
