package com.marketpay.spc.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse implements Serializable {
    private static final long serialVersionUID = 4272164774494426902L;

    private String mensagem;
    private List<String> erros;

    public GenericResponse(String mensagem, List<String> erros) {
        super();
        this.mensagem = mensagem;
        this.erros = erros;
    }
    public GenericResponse(String mensagem) {
        super();
        this.mensagem = mensagem;
    }
    public GenericResponse() {
        super();
    }
}