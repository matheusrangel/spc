package com.marketpay.spc.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LayoutCadastroResponse {

    private Long id;
    private String description;
    private String lineLength;

}
