package com.marketpay.spc.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.marketpay.spc.domain.Sftp;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArquivoCadastroRequest {

    @NotEmpty
    private String description;

    @NotEmpty
    private String originFilenameRegex;

    @NotEmpty
    private String destFilename;

    @NotEmpty
    private String originDir;

    @NotEmpty
    private String destDir;

    @NotNull
    private Long idOriginSftp;

    @NotNull
    private Long idDestSftp;

    @NotNull
    private Long idLayout;

}
