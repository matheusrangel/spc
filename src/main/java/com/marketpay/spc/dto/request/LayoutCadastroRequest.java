package com.marketpay.spc.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LayoutCadastroRequest {

    @NotEmpty
    private String description;

    @NotNull
    private Integer lineLength;

}
