package com.marketpay.spc.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SftpCadastroRequest implements Serializable {

    @NotNull
    private String host;

    @NotNull
    private String port;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String directory;

}
