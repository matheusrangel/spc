package com.marketpay.spc.controller;

import com.marketpay.spc.dto.request.LayoutCadastroRequest;
import com.marketpay.spc.service.LayoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/layout")
@Validated
public class LayoutController {

    @Autowired
    private LayoutService layoutService;

    @PostMapping
    public ResponseEntity cadastrar(
            @Valid @RequestBody LayoutCadastroRequest request) {
        return ResponseEntity.ok(layoutService.cadastrar(request));
    }

}
