package com.marketpay.spc.controller;

import com.marketpay.spc.domain.Arquivo;
import com.marketpay.spc.dto.request.ArquivoCadastroRequest;
import com.marketpay.spc.exception.RecursoNaoEncontradoException;
import com.marketpay.spc.service.ArquivoService;
import com.marketpay.spc.service.MessageService;
import com.marketpay.spc.service.ValidadorSpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/arquivo")
@Validated
public class ArquivoController {

    @Autowired
    private ValidadorSpcService spcJobService;

    @Autowired
    private ArquivoService arquivoService;

    @Autowired
    private MessageService messageService;

    @PostMapping
    public ResponseEntity cadastrar(
            @Valid @RequestBody ArquivoCadastroRequest request) {
        return ResponseEntity.ok(arquivoService.cadastrar(request));
    }

    @GetMapping("/{id}/valido")
    public ResponseEntity arquivoValido(
            @PathVariable("id") Long idArquivo)
            throws Exception {
        Arquivo arquivo = arquivoService
                .findById(idArquivo)
                .orElseThrow(() -> new RecursoNaoEncontradoException(
                        messageService.getMessage("not-found.arquivo")));

        spcJobService.executarRotina(arquivo);
        return ResponseEntity.ok().build();
    }
}
