package com.marketpay.spc.controller;

import com.marketpay.spc.dto.request.SftpCadastroRequest;
import com.marketpay.spc.service.SftpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/sftp")
@Validated
public class SftpController {

    @Autowired
    private SftpService sftpService;

    @PostMapping
    public ResponseEntity cadastrar(
            @Valid @RequestBody SftpCadastroRequest request) {
        return ResponseEntity.ok(sftpService.cadastrar(request));
    }

}
