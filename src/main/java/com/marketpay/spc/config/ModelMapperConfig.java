package com.marketpay.spc.config;

import com.marketpay.spc.dto.mapping.ArquivoRequestMapping;
import com.marketpay.spc.dto.mapping.ArquivoResponseMapping;
import com.marketpay.spc.dto.mapping.LayoutRequestMapping;
import com.marketpay.spc.dto.mapping.SftpRequestMapping;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setSkipNullEnabled(true);
        modelMapper.addMappings(new SftpRequestMapping());
        modelMapper.addMappings(new LayoutRequestMapping());
        modelMapper.addMappings(new ArquivoRequestMapping());
        modelMapper.addMappings(new ArquivoResponseMapping());

        return modelMapper;
    }
}